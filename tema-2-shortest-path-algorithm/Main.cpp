#include "MapGraph.h"

int Point::count = 0;

int main()
{
	MapGraph mapGraph;

	Point zero(1, 2, "Moreni");
	Point one(1, 4, "Brasov");
	Point two(2, 6, "Codlea");
	Point three(3, 2, "Constanta");
	Point four(4, 3, "Margineni");
	Point five(5, 5, "Ploiesti");
	Point six(6, 3, "Mangalia");
	Point seven(7, 2, "Simera");
	Point eight(8, 8, "Cluj");

	mapGraph.addVertex(zero);
	mapGraph.addVertex(one);
	mapGraph.addVertex(two);
	mapGraph.addVertex(three);
	mapGraph.addVertex(four);
	mapGraph.addVertex(five);
	mapGraph.addVertex(six);
	mapGraph.addVertex(seven);
	mapGraph.addVertex(eight);

	mapGraph.addEdge(zero, one, 4);
	mapGraph.addEdge(zero, seven, 8);
	mapGraph.addEdge(one, seven, 11);
	mapGraph.addEdge(one, two, 8);
	mapGraph.addEdge(two, eight, 2);
	mapGraph.addEdge(two, three, 7);
	mapGraph.addEdge(two, five, 4);
	mapGraph.addEdge(three, five, 14);
	mapGraph.addEdge(three, four, 9);
	mapGraph.addEdge(four, five, 10);
	mapGraph.addEdge(five, six, 2);
	mapGraph.addEdge(six, eight, 6);
	mapGraph.addEdge(seven, eight, 7);
	mapGraph.addEdge(seven, six, 1);

	std::cout << mapGraph.graph;
	std::cout << "Numar Orase: " << mapGraph.getNumVertices() << std::endl;
	std::cout << "Numar Drumuri: " << mapGraph.getNumEdges() << std::endl;

	int dijkstraDistance;
	Point startPoint = zero;
	Point goalPoint = four;
	std::list<Point> pointPath = mapGraph.dijkstraBFFW(startPoint, goalPoint, dijkstraDistance);
	std::cout << "Distance from " << startPoint.name << " to " << goalPoint.name << ": " << dijkstraDistance << std::endl;
	std::cout << "Path: " << pointPath << std::endl;

	std::cin.get();
	return 0;
}