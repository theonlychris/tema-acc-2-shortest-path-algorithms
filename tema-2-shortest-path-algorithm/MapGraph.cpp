#include "MapGraph.h"

const double infinity = std::numeric_limits<double>::infinity();

bool MapGraph::addVertex(const Point& location)
{
	std::map <Point, std::vector<Edge>>::iterator it;
	it = graph.find(location);
	if (it != graph.end())
		return false;

	std::vector<Edge> temp;
	graph.insert(std::make_pair(location, temp));
	return true;
}

void MapGraph::addEdge(const Point& end1, const Point& end2, double length, const std::string& roadName)
{
	std::map <Point, std::vector<Edge>>::iterator it;
	it = graph.find(end1);
	if (it == graph.end())
	{
		std::cout << "Nu exista punctul sursa" << std::endl;
		return;
	}
	Edge temp(end2, length, roadName);
	it->second.push_back(temp);

	it = graph.find(end2);
	if (it == graph.end())
	{
		std::cout << "Nu exista punctul destinatie" << std::endl;
		it->second.pop_back();
		return;
	}
	Edge temp2(end1, length, roadName);
	it->second.push_back(temp2);
}

void MapGraph::createAdjencyMatrix()
{
	std::vector<int> tempVector(Point::getCount());
	for (int i = 0; i < Point::getCount(); ++i)
		adjencyMatrix.push_back(tempVector);

	for (const auto& it : graph)
	{
		for (const auto& edge : it.second)
		{
			adjencyMatrix[it.first.id][edge.destination.id] = edge.cost;
		}
	}
}

void MapGraph::createAdjencyList()
{
	adjencyList.resize(Point::getCount());
	for (const auto& it : graph)
	{
		for (const auto& edge : it.second)
		{
			adjencyList[it.first.id].push_back(edge);
		}
	}
}

int MapGraph::getNumVertices() const
{
	return graph.size();
}

int MapGraph::getNumEdges() const
{
	int edgesNr = 0;
	for (const auto& it : graph)
	{
		edgesNr += it.second.size();
	}
	return edgesNr / 2;
}

void MapGraph::getDijkstraAllPaths(const Point& startPoint, std::vector<double>& minDistance, std::vector<int>& previous)
{
	if (adjencyList.size() == 0)
		createAdjencyList();

	int source = startPoint.id;
	minDistance.clear();
	minDistance.resize(adjencyList.size(), infinity);
	minDistance[source] = 0;
	previous.clear();
	previous.resize(adjencyList.size(), -1);
	std::set<std::pair<int, double> > pointQueue;
	pointQueue.insert(std::make_pair(source, minDistance[source]));

	while (!pointQueue.empty())
	{
		int destination = pointQueue.begin()->first;
		double distance = pointQueue.begin()->second;

		pointQueue.erase(pointQueue.begin());

		const std::vector<Edge>& edges = adjencyList[destination];

		for (const auto& edge : edges)
		{
			double newDistance = distance + edge.cost;
			if (newDistance < minDistance[edge.destination.id])
			{
				pointQueue.erase(std::make_pair(edge.destination.id, minDistance[edge.destination.id]));

				minDistance[edge.destination.id] = newDistance;
				previous[edge.destination.id] = destination;

				pointQueue.insert(std::make_pair(edge.destination.id, minDistance[edge.destination.id]));
			}
		}
	}
}

std::list<Point> MapGraph::getDijkstraPathToGoal(const Point& goalPoint, const std::vector<int>& previous)
{
	auto getPoint = [&](int pointId)
	{
		for (const auto& mapIter : graph)
		{
			if (mapIter.first.id == pointId)
				return mapIter.first;
		}
	};

	int point = goalPoint.id;
	std::list<Point> pointPath;
	while (point != -1)
	{
		pointPath.push_front(getPoint(point));
		point = previous[point];
	}
	return pointPath;
}

std::list<Point> MapGraph::aStarSearch(const Point& startPoint, const Point& goalPoint)
{
	return std::list<Point>();
}

std::list<Point> MapGraph::dijkstraBFFW(const Point& startPoint, const Point& goalPoint, int& distance)
{
	std::vector<double> minDistance;
	std::vector<int> previous;
	getDijkstraAllPaths(startPoint, minDistance, previous);
	std::list<Point> pointPath = getDijkstraPathToGoal(goalPoint, previous);
	distance = minDistance[goalPoint.id];
	return pointPath;
}

std::ostream& operator<<(std::ostream& os, const MapGraph& mapGraph)
{
	os << mapGraph.graph;
	return os;
}
