#include "Edge.h"

Edge::Edge(const Point& destination, int cost, const std::string& roadName) : destination(destination), cost(cost), roadName(roadName)
{
	// Empty
}

std::ostream& operator<<(std::ostream& os, const Edge& edge)
{
	//os << "[" << edge.source << ", " << edge.destination << ", " << edge.cost << "]";
	os << "[" << edge.destination.id << ", " << edge.cost << "]";
	//os << edge.destination;
	return os;
}


