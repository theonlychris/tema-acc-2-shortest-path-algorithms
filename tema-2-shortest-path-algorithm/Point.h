#pragma once
#include <iostream>
#include <string>

class Point
{
public:
	Point(int x, int y, const std::string& name = "Undefined Name");
	~Point() = default;
public:
	int x;
	int y;
	std::string name;
	int id;
public:
	static int getCount();
public:
	friend	std::ostream& operator << (std::ostream& os, const Point& point);
	friend  bool operator != (const Point& pointOne, const Point& pointTwo);
public:
	struct Comparator
	{
		bool operator()(const Point& pointOne, const Point& pointTwo) const
		{
			if (pointOne.x < pointTwo.x) return true;
			if (pointOne.x > pointTwo.x) return false;
			if (pointOne.y < pointTwo.y) return true;
			return false;
		}
	};
private:
	static int count;
};

