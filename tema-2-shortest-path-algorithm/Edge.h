#pragma once
#include <memory>
#include "Point.h"
class Edge
{
public:
	Edge(const Point& destination, int cost, const std::string& roadName = "Undefined Name");
	~Edge() = default;
public:
	Point destination;
	int cost;
	std::string roadName;
public:
	friend	std::ostream& operator << (std::ostream& os, const Edge& edge);
};

