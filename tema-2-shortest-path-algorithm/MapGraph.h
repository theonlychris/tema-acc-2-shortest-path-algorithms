#pragma once
#include <map>
#include <memory>
#include <vector>
#include <list>
#include <limits> // for numeric_limits
#include <set>
#include <utility> // for pair
#include <algorithm>
#include <iterator>
#include "Point.h"
#include "Edge.h"
#include "Utils.hpp"

class MapGraph
{
public:
	MapGraph() = default;
	~MapGraph() = default;
public:
	bool addVertex(const Point& location);
	void addEdge(const Point& end1, const Point& end2, double length, const std::string& roadName = "Undefined Name");
	void createAdjencyMatrix();
	void createAdjencyList();
	int getNumVertices() const;
	int getNumEdges() const;
public:
	void getDijkstraAllPaths(const Point& startPoint, std::vector<double>& minDistance, std::vector<int>& previous);
	std::list<Point> getDijkstraPathToGoal(const Point& goalPoint, const std::vector<int>& previous);

	std::list<Point> aStarSearch(const Point& startPoint, const Point& goalPoint);
	std::list<Point> dijkstraBFFW(const Point& startPoint, const Point& goalPoint, int& distance);
public:
	friend std::ostream& operator << (std::ostream& os, const MapGraph& mapGraph);
public:
	std::map<Point, std::vector<Edge>,Point::Comparator> graph;
	std::vector<std::vector<int>> adjencyMatrix;
	std::vector<std::vector<Edge>> adjencyList;
};

