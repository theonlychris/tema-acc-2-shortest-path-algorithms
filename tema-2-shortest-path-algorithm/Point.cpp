#include "Point.h"



Point::Point(int x, int y, const std::string& name) : x(x), y(y), name(name), id(count++)
{
	// Empty
}

int Point::getCount()
{
	return count;
}

std::ostream& operator<<(std::ostream& os, const Point& point)
{
	//os << "(" << point.id << ", " << point.name << ", " << point.x << ", " << point.y << ")";
	os << "[" << point.id << ", " << point.name << "]";
	return os;
}

bool operator!=(const Point& pointOne, const Point& pointTwo)
{
	return !(pointOne.x == pointTwo.x && pointOne.y == pointTwo.y && pointOne.name == pointTwo.name);
}
