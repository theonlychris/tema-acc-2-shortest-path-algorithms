#ifndef UTILS_HPP
#define UTILS_HPP
#include <iostream>
#include <vector>

/* Overloads the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<T>& myVector)
{
	os << "{ ";
	for (int i = 0; i < myVector.size(); ++i) {
		os << myVector[i];
		if (i != myVector.size() - 1)
			os << ", ";
	}
	os << " }" << std::endl;
	return os;
}

/* Overloads the << Operator to show the contents of a map */
template <typename T, typename S, typename Comparable>
std::ostream& operator<<(std::ostream & os, const std::map<T, S, Comparable> & myMap)
{
	for (const auto& it : myMap)
	{
		os << it.first << " : "
			<< it.second << "\n";
	}

	return os;
}

/* Overloads the << Operator to show the contents of a matrix */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<std::vector<T>>& myMatrix)
{
	for (int i = 0; i < myMatrix.size(); ++i)
	{
		for (int j = 0; j < myMatrix[i].size(); ++j)
		{
			os << myMatrix[i][j] << " ";
		}
		os << std::endl;
	}
	return os;
}

/* Overloads the << Operator to show the contents of a list */
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::list<T>& myList)
{
	if (!myList.empty())
		std::copy(myList.begin(), myList.end(), std::ostream_iterator<T>(os, " - "));
	return os;
}

#endif // !UTILS_HPP
